# QA Engineer Interview

## CSS Selector
What is the most reliable way to select the element with "EUR 200.000 +" as text?

```html
<div class="row">
    <div class="col-md" data-animation-style="fadeIn" data-animation-delay="0" data-animation-duration="1.3">
        <div class="pin pin-s pin-left-first">
            <svg class="icon icon-pin"><use xlink:href="#icon-pin"></use></svg>
            <p>Head of Controlling</p>
            <p class="salary">EUR 80.000 - 100.000</p>
            <div class="source"><span><span>Source: company</span></span></div>
        </div>
        <div class="pin pin-ml pin-left-second">
            <svg class="icon icon-pin"><use xlink:href="#icon-pin"></use></svg>
            <p>Head of Purchasing</p>
            <p class="salary">EUR 100.000 - 125.000</p>
            <div class="source"><span><span>Source: job portal</span></span></div>
        </div>
        <div class="pin pin-sm pin-left-third">
            <svg class="icon icon-pin"><use xlink:href="#icon-pin"></use></svg>
            <p>Head of IT</p>
            <p class="salary">EUR 100.000 - 125.000</p>
            <div class="source"><span><span>Source: company</span></span></div>
            <div class="line"></div>
        </div>
        <div class="pin pin-ml pin-left-fourth">
            <svg class="icon icon-pin"><use xlink:href="#icon-pin"></use></svg>
            <p>Head of Marketing</p>
            <p class="salary">EUR 100.000 - 125.000</p>
            <div class="source"><span><span>Source: headhunter</span></span></div>
        </div>
    </div>
    <div class="col-md" data-animation-style="fadeIn" data-animation-delay="0" data-animation-duration="1.3">
        <div class="pin pin-s pin-right-first">
            <svg class="icon icon-pin"><use xlink:href="#icon-pin"></use></svg>
            <p>Managing Director SME</p>
            <p class="salary">EUR 200.000 +</p>
            <div class="source"><span><span>Source: headhunter</span></span></div>
        </div>
        <div class="pin pin-m pin-right-second">
            <svg class="icon icon-pin"><use xlink:href="#icon-pin"></use></svg>
            <p>CFO / General Manager</p>
            <p class="salary">EUR 125.000 - 150.000</p>
            <div class="source"><span><span>Source: headhunter</span></span></div>
        </div>
        <div class="pin pin-m pin-right-third">
            <svg class="icon icon-pin"><use xlink:href="#icon-pin"></use></svg>
            <p>Head of Sales</p>
            <p class="salary">EUR 125.000 - 150.000</p>
            <div class="source"><span><span>Source: company</span></span></div>
        </div>
        <div class="pin pin-l pin-right-fourth">
            <svg class="icon icon-pin"><use xlink:href="#icon-pin"></use></svg>
            <p>Head of Supply Chain</p>
            <p class="salary">EUR 100.000 - 125.000</p>
            <div class="source"><span><span>Source: headhunter</span></span></div>
        </div>
    </div>
</div>
```

-----

## Database

Given the 2 following tables:

Table 'user'
| id | user_firstname | user_lastname |
| - | - | - | 
| 1 | John | Smith |
| 2 | John | Schmidt |
| 3 | Maria | Dupont |
| 4 | Maria | Smith |
| 5 | John | Lucas |

Table 'user_address'
|user_id|city|country_code|
| - | - | - | 
|1|Hamburg|US|
|2|Hamburg|DE|
|3|Paris|FR|
|4|New York|US|
|5|Chicago|US| 

What value will the following SQL return?
 
```
SELECT COUNT(DISTINCT t1.id) from user t1
    JOIN user_address t2 on t1.id = t2.user_id
    WHERE t1.user_firstname LIKE 'John' and t2.country_code LIKE 'US'
```

-----