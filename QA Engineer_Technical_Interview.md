# Technical Interview QA Engineer

## Intro
(10 min)
- Overview about the next two and half hours
- JobLeads Crews/Divisions
- Get to know with short presentation
- Please make notes for all tasks so that you can do short presentation in the end.

## Tasks

### Design
(5 min)

What would you change? Do you see any design/layout issues when looking at the screenshot?

![Offer](ui_diff.png)

-----

### Web information
(10 min)

Call [https://stage.jobleads.com/](https://stage.jobleads.com/)

- What file _size_ does the ```app.js``` have and what is the _value_ of version parameter?
- What is the _value_ of the ```locale``` cookie?
- What value has the _referer_ for the ```logo_JL.svg```?
- What value has the _color_ and _font-size_ for ```Academy``` link in the header?

-----

### Test Automation
(30 min)

Write an automated test to search jobs by *Job title* and by *City* on [https://stage.jobleads.com/](https://stage.jobleads.com/) and verify that the search results match the criteria. 
If you don't have an IDE to use, it's also fine to write this down as pseudo-code. The important thing is to see how you would access the elements and how you would check the results.

-----

### Bug report
(10 min)

Following scenario: 
You are logged in as Premium User and you want to search a Headhunter by name. You know that this Headhunter exists but the Headhunter is not displayed in the search results.  

Write a short bug report. What information would you give to the developers?

![Headhunter search](hh_search_error.png)

-----

### Test Scenarios
(15 min) 

When you look at these two pages, what test scenarios come to your mind? By clicking _Order now_ or _Select this package_ on the first page the user gets to the second page. 

Please summarize in bullet points what should be covered by tests.

1. Page

![Offer](offer.png)

2. Page

![Payment](payment.png)

-----

### XML
(10 min)

Please have a look at the following xml file. Do you notice anything? Please look at the format.

```xml
<?xml version='1.0' encoding='utf-99' standalone='yes'?>
<root>
    <resultlist>
        <beruf>
            <city>Lüneburg</city>
            <company_description><![CDATA[JL QA &amp; Co. KG description üäöß]]></company_description>
            <company_name>JL QA &amp; Co. KG</company_name>
            <company_type value="1">Direct employer</company_type>
            <country>DE</country>
            <date_of_last_change>___date___</date_of_last_change>
            <industry>46</industry>
            <jobdatum>___date___</jobdatum>
            <job_description><![CDATA[Cypress <b>Test Automation</b> Manager
                JL QA &amp; Co. KG testet Software mit Cypress - job description]]></job_description>
            <job_endtext><![CDATA[JL QA &amp; Co. KG in Lüneburg - Endtext]]></job_endtext>
            <job_id>___id___</job_id>
            <job_title>Cypress Test Automation Manager (___id___)</job_title>
            <profile_description><![CDATA[Abgeschlossenes technisches Studium oder eine vergleichbare Qualifikation. üäö€ß - Profile]]></profile_description>
            <region value="NI">Niedersachsen</region>
            <url>https://fixtures.jobleads.com/jobs/jl-qa-automation/nonajax-job-details-page-2.html?id=___id___</url>
            <zip_code>21335</zip_code>
        </job> 
        <job>
            <source_type value="4">Company site &amp; other</source_type>
            <region value="NI">Niedersachsen</region>
            <date_of_last_change>___date___</date_of_last_change>
            <job_id>___id2___</job_id>
            <job_title>Freiberuflicher Cypress Manager (___id2___)</job_title>
            <company_name>JL QA &amp; Co. KG</company_name>
            <url>https://fixtures.jobleads.com/jobs/jl-qa-automation/nonajax-job-details-page-2.html?id=___id2___</url>
            <city>Lüneburg</city>
            <zip_code>21335</zip_code>
            <company_description>JL QA &amp; Co. KG description üäöß</company_description>
            <task_description>Job Title Blocklist test</task_description>
            <jobdatum>___date___</jobdatum>
            <company_type value="1">Direct employer</company_type>
            <country>DE</country>
            <industry></industry>
            <job_endtext>JL QA &amp; Co. KG in Lüneburg</job_endtext>
        </job>
    </resultlist>
```

----- 

## Presentation of the results / Get to know the team
(45 min)